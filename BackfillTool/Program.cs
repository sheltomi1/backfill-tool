﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using FileHelpers.Events;


namespace BackfillTool
{
 

    public class Program
    {

        static void Main(string[] args)
        {

            //1. Put the Backfill files that sdl have generated in a directory, should be one file each for RAQ, Registration, WebActivity. Set the location of the whole lot here:
            string location = "D:\\Mike\\China Backfill 23 Jan to 28 Jan 2022";

            string backfillFilesFolder = "Backfill";
            string existingCsvFilesFolder = "Existing";

            string raqBackfillFile = location + "\\" + backfillFilesFolder + "\\" + "RequestAQuote.csv";
            string registrationBackfillFile = location + "\\" + backfillFilesFolder + "\\" + "Registration.csv";
            string webActivityBackfillFile = location + "\\" + backfillFilesFolder + "\\" + "WebActivity.csv";

            //2. Put the existing csv files, the data that SAP already has, into a separate directory called "Existing"
            string existingCsvFilesLocation = location + "\\" + existingCsvFilesFolder;

            //3. Set location where you want the output files to go
            string outputFilesLocation = location;

            ProcessRegistrations(registrationBackfillFile, existingCsvFilesLocation, outputFilesLocation);
            ProcessRAQ(raqBackfillFile, existingCsvFilesLocation, outputFilesLocation);
            //ProcessWebActivity(webActivityBackfillFile, existingCsvFilesLocation, outputFilesLocation);
        }


        private static void ProcessWebActivity(string webActivityBackfillFile, string existingCsvFilesLocation, string outputFilesLocation)
        {

            List<WebActivity> outputWebActivity = new List<WebActivity>();
            var engine = new FileHelperEngine<WebActivity>();
            engine.BeforeReadRecord += BeforeEventWebActivity;
            var webActivityBackfill = engine.ReadFile(webActivityBackfillFile);
            var existingCsvFilesRegistration = Directory.GetFiles(existingCsvFilesLocation).Where(x => x.Contains("WebActivity")).ToList();


            List<WebActivityFile> listExistingCsvFilesRegistration = new List<WebActivityFile>();

            foreach (var csvFile in existingCsvFilesRegistration)
            {
                var thisCsvFile = engine.ReadFile(csvFile);
                listExistingCsvFilesRegistration.Add(new WebActivityFile() { Filename = csvFile, WebActivityArray = thisCsvFile });
            }

            foreach (var webActivityBackfillEntry in webActivityBackfill)
            {
                int count = 0;
                foreach (var existingCsvFile in listExistingCsvFilesRegistration)
                {
                    if (IsEntryInWebActivityBackfillFile(webActivityBackfillEntry, existingCsvFile))
                    {
                        count++;
                    }
                }
                if (count == 0)
                    outputWebActivity.Add(webActivityBackfillEntry);
            }
            engine.WriteFile(outputFilesLocation + "\\WebActivities", outputWebActivity);
        }


        private static void ProcessRegistrations(string registrationBackfillFile, string existingCsvFilesLocation, string outputFilesLocation)
        {
            List<Registration> outputRegistrations = new List<Registration>();
            var engine = new FileHelperEngine<Registration>();
            engine.BeforeReadRecord += BeforeEventRegistration;
            var registrationBackfill = engine.ReadFile(registrationBackfillFile);
            var existingCsvFilesRegistration = Directory.GetFiles(existingCsvFilesLocation).Where(x => x.Contains("Registration")).ToList();

            List<RegistrationFile> listExistingCsvFilesRegistration = new List<RegistrationFile>();

            foreach (var csvFile in existingCsvFilesRegistration)
            {
                var thisCsvFile = engine.ReadFile(csvFile);
                listExistingCsvFilesRegistration.Add(new RegistrationFile() { Filename = csvFile, RegistrationArray = thisCsvFile });
            }

            foreach (var registrationBackfillEntry in registrationBackfill)
            {
                int count = 0;
                foreach (var existingCsvFile in listExistingCsvFilesRegistration)
                {
                    if (IsEntryInRegistrationBackfillFile(registrationBackfillEntry, existingCsvFile))
                    {
                        count++;
                    }
                }
                if(count == 0)
                    outputRegistrations.Add(registrationBackfillEntry);
            }
            engine.WriteFile(outputFilesLocation + "\\Registrations", outputRegistrations);
        }

        private static void ProcessRAQ(string raqBackfillFile, string existingCsvFilesLocation, string outputFilesLocation)
        {
            List<Raq> outputRAQs = new List<Raq>();
            var engine = new FileHelperEngine<Raq>();
            engine.BeforeReadRecord += BeforeEventRaq;
            var raqBackfill = engine.ReadFile(raqBackfillFile);
            var existingCsvFilesRAQ = Directory.GetFiles(existingCsvFilesLocation).Where(x => x.Contains("RequestAQuote")).ToList();

            List<RaqFile> listExistingCsvFilesRegistration = new List<RaqFile>();

            foreach (var csvFile in existingCsvFilesRAQ)
            {
                var thisCsvFile = engine.ReadFile(csvFile);
                listExistingCsvFilesRegistration.Add(new RaqFile() { Filename = csvFile, RaqArray = thisCsvFile });
            }

            foreach (var raqBackfillEntry in raqBackfill)
            {
                //int fred2;
                //if (raqBackfillEntry.EMAIL.Contains("lichuxin"))
                //    fred2 = 1;

                int count = 0;
                foreach (var existingCsvFile in listExistingCsvFilesRegistration)
                {
                    if (IsEntryInRaqBackfillFile(raqBackfillEntry, existingCsvFile))
                    {
                        count++;
                    }
                }
                if (count == 0)
                    outputRAQs.Add(raqBackfillEntry);
            }
            engine.WriteFile(outputFilesLocation + "\\RAQs", outputRAQs);
        }

        private static void BeforeEventWebActivity(EngineBase engine, BeforeReadEventArgs<WebActivity> e)
        {
            var line = e.RecordLine;
            var fixedLine = ReplaceEmbeddedCommasAndQuotesWithSomethingDifferent(line);
            e.RecordLine = fixedLine; // replace the line with the fixed version
        }


        private static void BeforeEventRegistration(EngineBase engine, BeforeReadEventArgs<Registration> e)
        {
            var line = e.RecordLine;
            var fixedLine = ReplaceEmbeddedCommasAndQuotesWithSomethingDifferent(line);
            e.RecordLine = fixedLine; // replace the line with the fixed version
        }

        private static void BeforeEventRaq(EngineBase engine, BeforeReadEventArgs<Raq> e)
        {
            var line = e.RecordLine;
            var fixedLine = ReplaceEmbeddedCommasAndQuotesWithSomethingDifferent(line);
            e.RecordLine = fixedLine; // replace the line with the fixed version
        }



        private static string ReplaceEmbeddedCommasAndQuotesWithSomethingDifferent(string line)
        {
            return ParseCommasInQuotes(line);
        }

        private static string ParseCommasInQuotes(string arg)
        {
            StringBuilder output = new StringBuilder();
            int endOfFieldNumber = 0;

            //44 = comma
            //34 = double quote

            for (int x = 0; x < arg.Length; x++)
            {
                char thisElement = arg[x];
                bool previousElementWasQuote = false;
                bool nextElementIsQuote = false;
                bool previousElementWasComma = false;
                bool nextElementIsComma = false;



                if (thisElement.Equals((Char)44))
                {
                    endOfFieldNumber++;

                    previousElementWasQuote = ((x >= 1) && (arg[x-1].Equals((Char)34)))? true : false;
                    nextElementIsQuote = ((x < arg.Length-2) && (arg[x + 1].Equals((Char)34))) ? true : false;
                    previousElementWasComma = ((x >= 1) && (arg[x - 1].Equals((Char)44))) ? true : false;
                    nextElementIsComma = ((x < arg.Length - 2) && (arg[x + 1].Equals((Char)44))) ? true : false;

                    if (previousElementWasQuote && nextElementIsQuote)
                        output.Append(thisElement); //don't replace
                    else if (previousElementWasComma && nextElementIsComma )
                        output.Append(thisElement);
                    else
                        output.Append('*');

                }
                else
                {
                    output.Append(thisElement);
                }
            }
            string amendedLine = output.ToString();
            return amendedLine;
        }




        private static string ParseCommasInQuotes2(string arg)
        {
            bool foundEndQuote = false;
            bool foundStartQuote = false;
            StringBuilder output = new StringBuilder();

            //44 = comma
            //34 = double quote

            for(int x = 0; x <= arg.Length; x++)
            {
                char element = arg[x];
                if (foundEndQuote)
                {
                    foundStartQuote = false;
                    foundEndQuote = false;
                }

                if (element.Equals((Char)34) & (!foundEndQuote) & foundStartQuote)
                {
                    foundEndQuote = true;
                    continue;
                }

                if (element.Equals((Char)34) & !foundStartQuote)
                {
                    foundStartQuote = true;
                    continue;
                }

                if (element.Equals((Char)44) & foundStartQuote)
                {
                    //skip the comma...its between double quotes
                }
                else
                {
                    output.Append(element);
                }
            }
            return output.ToString();
        }


        private static bool IsEntryInRegistrationBackfillFile(Registration registrationBackfillEntry, RegistrationFile existingCsvFile)
        {
            var existingCsvFileArray = existingCsvFile.RegistrationArray;

            var fred = existingCsvFileArray.Where(x => x.EMAIL == registrationBackfillEntry.EMAIL).SingleOrDefault();

            if (fred != null)
                return true;
            else return false;
        }

        private static bool IsEntryInRaqBackfillFile(Raq raqBackfillEntry, RaqFile existingCsvFile)
        {
            var existingCsvFileArray = existingCsvFile.RaqArray;
            var fred = existingCsvFileArray.Any(x => x.EMAIL == raqBackfillEntry.EMAIL && x.SUBMITTED == raqBackfillEntry.SUBMITTED && x.FORM_TYPE == raqBackfillEntry.FORM_TYPE);

            return fred;
        }

        private static bool IsEntryInWebActivityBackfillFile(WebActivity webActivityBackfillEntry, WebActivityFile existingCsvFile)
        {
            var existingCsvFileArray = existingCsvFile.WebActivityArray;
            var fred = existingCsvFileArray.Any(x => x.EMAIL == webActivityBackfillEntry.EMAIL &&
                                                     x.TYPE_ID == webActivityBackfillEntry.TYPE_ID &&
                                                     x.DATE == webActivityBackfillEntry.DATE);

            return fred;
        }


    }


    public class RegistrationFile
    {
        public string Filename { get; set; }
        public Registration[] RegistrationArray { get; set; }

    }

    public class RaqFile
    {
        public string Filename { get; set; }
        public Raq[] RaqArray { get; set; }

    }

    public class WebActivityFile
    {
        public string Filename { get; set; }
        public WebActivity[] WebActivityArray { get; set; }

    }



    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst]
    public class Registration
    {
        public string MUID;
        public string COUNTRY;
        public string CITY;
        public string INDUSTRY;
        public string CREATED;
        public string INTEREST;
        public string MODEL;
        public string CUSTOMER;
        public string COMPANY;
        public string EMAIL;
        public string SOURCE;
        public string REG_TYPE;
        public string CONTACT;
        public string JOBTITLE;
        public string DEPARTMENT;
        public string TITLE;
        public string FORENAME;
        public string SURNAME;
        public string TEL;
        public string SUB_STATUS;

    }

    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst]
    
    public class Raq
    {

        public string MUID;
        public string COUNTRY;
        public string CITY;
        public string INDUSTRY;
        public string CREATED;
        public string INTEREST;
        public string OPPORTUNITYTYPE;
        public string CUSTOMER;
        public string COMPANY;
        public string EMAIL;
        public string SOURCE;
        public string REG_TYPE;
        public string CONTACT;
        public string JOBTITLE;
        public string DEPARTMENT;
        public string TITLE;
        public string FORENAME;
        public string SURNAME;
        public string TEL;
        [FieldQuoted('"', QuoteMode.OptionalForRead, MultilineMode.AllowForBoth)]
        public string APPLICATION_DETAILS;
        public string SUBMITTED;
        public string FORM_TYPE;

    }

    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst]
    public class WebActivity
    {

        public string MUID;
        public string TYPE_ID;
        public string DATE;
        public string EVENT_DATE;
        public string EMAIL;
        public string PUBLICATION_ID;

    }



}
